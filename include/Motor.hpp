#pragma once

#include <Arduino.h>

class Servo;

class Motor {
public:
    Motor(int lower, int upper);
    void init(int pin, bool reversed);
    virtual int setPower(int power, double maxPercentage) = 0;
    virtual void stop() = 0;

protected:
    /*
     * Power is on the range 0.0 to 1.0, where 0.0 corresponds to the lower bound and 1.0 corresponds to the upper bound
     */
    int writePower(double power);

    bool reversed;

private:
    Servo* servo;
    int range;
    int lower;
};

class Motor1d : public Motor{
public:
    Motor1d(int lower = 1100, int upper = 1900);
    int setPower(int power, double maxPercentage) override;
    void stop() override;
};

class Motor2d : public Motor{
public:
    Motor2d(int lower = 1100, int upper = 1900);

    int setPower(int power, double maxPercentage) override;
    void stop() override;
};
