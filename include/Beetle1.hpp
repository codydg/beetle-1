#pragma once

#define BEETLE_1

#include <Arduino.h>

#include "Motor.hpp"

class Beetle1 {
public:
    Beetle1(double powerPerTime = 0.25);
    void init(int leftPin, int rightPin, int weaponPin);

    String update(bool enable, double maxWeaponPower, int forwards, int turn, bool weaponEnable);
    void stop();

private:
    Motor2d left;
    Motor2d right;
    Motor1d weapon;

    double weaponPower = 0.0;
    double lastTimeCheck = 0.0;
    const double powerPerTime;

    static int writeMotor2d(int power, double maxPower, Servo& servo, bool reversed);
    static int writeMotor1d(int power, double maxPower, Servo& servo, bool reversed);
    static double getTime_s();
};
