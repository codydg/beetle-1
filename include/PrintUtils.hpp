#pragma once

#include <Arduino.h>

#ifndef USE_SERIAL
#define USE_SERIAL true
#endif

namespace PrintUtils {
    void init(unsigned long baud) {
    #if USE_SERIAL
        Serial.begin(baud);
    #endif
    }

    template<typename T>
    void print(const T& toPrint) {
    #if USE_SERIAL
        Serial.print(toPrint);
    #endif
    }

    template<typename T>
    void println(const T& toPrint) {
    #if USE_SERIAL
        Serial.println(toPrint);
    #endif
    }

    void println() {
        println("");
    }
}
