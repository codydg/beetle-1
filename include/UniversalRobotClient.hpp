#pragma once

#include <Arduino.h>
#include <RF24.h>

struct DataPacket {
    int left_x;
    int left_y;
    bool left_b;

    int right_x;
    int right_y;
    bool right_b;

    bool toggle_1;
    bool toggle_2;
    bool toggle_3;
    bool toggle_4;

    bool blue_b;
    bool red_b;
};

class UniversalRobotClient {
public:
    UniversalRobotClient(uint16_t ce, uint16_t csn, uint64_t address = 0xE7E7E7E6E6E6, uint8_t pipe = 1);
    void init();
    bool read(DataPacket& packet);
    void zero(const DataPacket& packet);

    void setDeadzone(int deadzone) { this->deadzone = deadzone; }
    int getDeadzone() { return deadzone; }

private:
    RF24* radio;
    int16_t buffer[12];
    const uint8_t pipe;
    const uint64_t address;
    int deadzone = 0;
    int zeroLeftX = 0;
    int zeroLeftY = 0;
    int zeroRightX = 0;
    int zeroRightY = 0;

    int adjustJoystick(int raw, int center);
};
