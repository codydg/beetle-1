#include "Motor.hpp"

#include <Servo.h>

Motor::Motor(int lower, int upper) {
    this->range = upper - lower;
    this->lower = lower;
}

void Motor::init(int pin, bool reversed) {
    servo = new Servo();
    servo->attach(pin);

    this->reversed = reversed;
}

int Motor::writePower(double power) {
    int powerI = static_cast<int>(power * range + lower);
    servo->writeMicroseconds(powerI);
    return powerI;
}

Motor1d::Motor1d(int lower, int upper) : Motor(lower, upper) {}

int Motor1d::setPower(int power, double maxPercentage) {
    if (reversed) {
        power = 255 - power;
    }
    double powerD = power / 255.0;
    powerD = min(max(0.0, power), maxPercentage);
    return writePower(powerD);
}

void Motor1d::stop() {
    writePower(0.0);
}

Motor2d::Motor2d(int lower, int upper) : Motor(lower, upper) {}

int Motor2d::setPower(int power, double maxPercentage) {
    if (reversed) {
        power = -power;
    }
    double powerD = power / 255.0;
    powerD = min(max(-maxPercentage, power), maxPercentage);
    return writePower(powerD*0.5 + 0.5);
}

void Motor2d::stop() {
    writePower(0.5);
}
