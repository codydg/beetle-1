#include "UniversalRobotClient.hpp"

UniversalRobotClient::UniversalRobotClient(uint16_t ce, uint16_t csn, uint64_t address, uint8_t pipe)
    : pipe(pipe), address(address) {
    radio = new RF24(ce, csn);
}

void UniversalRobotClient::init() {
    radio->begin();
    //radio.openReadingPipe(PIPE, address);
    radio->openReadingPipe(pipe, address);
    radio->startListening();
}

bool UniversalRobotClient::read(DataPacket& packet) {
    if (!radio->available()) {
        return false;
    }

    radio->read(buffer, sizeof(buffer));

    packet.left_x = adjustJoystick(buffer[0], zeroLeftX);
    packet.left_y = adjustJoystick(buffer[1], zeroLeftY);
    packet.right_x = adjustJoystick(buffer[2], zeroRightX);
    packet.right_y = adjustJoystick(buffer[3], zeroRightY);
    packet.left_b = buffer[4];
    packet.right_b = buffer[5];
    packet.toggle_1 = buffer[6];
    packet.toggle_2 = buffer[7];
    packet.toggle_3 = buffer[8];
    packet.toggle_4 = buffer[9];
    packet.blue_b = buffer[10];
    packet.red_b = buffer[11];

    return true;
}

void UniversalRobotClient::zero(const DataPacket& packet) {
    zeroLeftX = packet.left_x;
    zeroLeftY = packet.left_y;
    zeroRightX = packet.right_x;
    zeroRightY = packet.right_y;
}

int UniversalRobotClient::adjustJoystick(int raw, int center) {
    if (raw > center) {
        raw = map(raw, center, 255, 0, 255);
    } else {
        raw = map(raw, -255, center, -255, 0);
    }
    if (abs(raw) < deadzone) {
        return 0;
    } else {
        return raw;
    }
}
