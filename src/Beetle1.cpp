#include "Beetle1.hpp"

Beetle1::Beetle1(double powerPerTime) : left(500, 2500),
                                        right(500, 2500),
                                        weapon(1000, 2000),
                                        powerPerTime(powerPerTime) {}

void Beetle1::init(int leftPin, int rightPin, int weaponPin) {
    left.init(leftPin, false);
    right.init(rightPin, true);
    weapon.init(weaponPin, false);

    delay(20);
    weapon.setPower(255, 1.0);
    delay(20);
    stop();
    lastTimeCheck = getTime_s();
}

String Beetle1::update(bool enable, double maxWeaponPower, int forwards, int turn, bool weaponEnable) {
    int leftP   = enable ? (forwards + turn) : 0;
    int rightP  = enable ? (forwards - turn) : 0;
    int weaponP = enable && weaponEnable ? 255 : 0;

    double time = getTime_s();
    weaponPower += (time - lastTimeCheck) * powerPerTime;
    weaponPower = min(weaponPower, weaponEnable ? maxWeaponPower : 0.0);

    lastTimeCheck = time;
    leftP = left.setPower(leftP, 1.0);
    rightP = right.setPower(rightP, 1.0);
    weaponP = weapon.setPower(weaponP, weaponPower);

    return "L: " + String(leftP) + " | R: " + String(rightP) + " | W: " + String(weaponP);
}

void Beetle1::stop() {
    left.stop();
    right.stop();
    weapon.stop();
}

double Beetle1::getTime_s() {
    return millis() / 1000.0;
}
