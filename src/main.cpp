#include <Arduino.h>

#define USE_SERIAL false
#include "PrintUtils.hpp"
#include "UniversalRobotClient.hpp"

#include "Beetle1.hpp"

UniversalRobotClient client(0, 1);
DataPacket packet;
unsigned long lastReadTime = 0;
unsigned long watchdogTimeout_ms = 200;

#define LEFT_PIN    3
#define RIGHT_PIN   4
#define WEAPON_PIN  5
Beetle1 robot;

void update(const DataPacket& packet);

void setup() {
    PrintUtils::init(115200);
    robot.init(LEFT_PIN, RIGHT_PIN, WEAPON_PIN);
    client.init();

    while (!client.read(packet)) {
        PrintUtils::println("Waiting for Radio Signal");
        delay(1000);
    }
    PrintUtils::println("Waiting for user to disable");
    while (packet.toggle_1) {
        client.read(packet);
    }
    PrintUtils::println("Waiting for user to enable");
    while (!packet.toggle_1) {
        client.read(packet);
    }
    client.zero(packet);
    client.setDeadzone(5);
}

void loop() {
    if (client.read(packet)) {
        // Packet Received
        lastReadTime = millis();
        update(packet);
    } else if (lastReadTime != 0 && millis() - lastReadTime > watchdogTimeout_ms) {
        // Lost connection
        PrintUtils::println("Lost Connection");
        robot.stop();
        lastReadTime = 0;
    }
    delay(10);
}

void update(const DataPacket& packet) {
    int forwards = packet.left_y;
    int turn = packet.right_x;

    // Scale to cap at 100% power
    int maxVal = max(abs(forwards), abs(turn));
    if (maxVal > 255) {
        // Cannot use *= due to integer division
        forwards = forwards * 255 / maxVal;
        turn = turn * 255 / maxVal;
    }

    // Divide by user's slow mode setting
    double maxWeaponPower = 1.0 / ((packet.toggle_3 ? 1.0 : 1.5) * (packet.toggle_4 ? 1.0 : 2.0));
    bool enable = packet.toggle_1;
    bool weaponEnable = packet.toggle_2;

    String toPrint = "Forwards: " +
                     String(forwards) +
                     " | Turn: " +
                     String(turn) +
                     " | Weapon: " +
                     (weaponEnable ? "On" : "Off");

    toPrint += " | " + robot.update(enable, maxWeaponPower, forwards, turn, weaponEnable);

    PrintUtils::println(toPrint);
}
